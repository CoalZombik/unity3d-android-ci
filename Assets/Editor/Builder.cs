using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.Build.Reporting;
using System;

public static class BuilderCI
{
	public static void BuildAndroid ()
	{
		Console.WriteLine ("==== PREPARE BUILD =====");
		//Get git info
		string branch = Environment.GetEnvironmentVariable ("CI_COMMIT_REF_NAME");
		string commit = Environment.GetEnvironmentVariable ("CI_COMMIT_SHORT_SHA");

		if (branch == null || commit == null)
			throw new Exception ("Get git info failed!");

		//Setup player settings
		PlayerSettings.bundleVersion = branch + "+" + commit;
		PlayerSettings.Android.useCustomKeystore = true;
		PlayerSettings.Android.keystoreName = Environment.GetEnvironmentVariable ("KEYSTORE_PATH");
		PlayerSettings.Android.keystorePass = Environment.GetEnvironmentVariable ("KEYSTORE_PASSWORD");
		PlayerSettings.Android.keyaliasName = Environment.GetEnvironmentVariable ("KEYSTORE_ALIAS");
		PlayerSettings.Android.keyaliasPass = Environment.GetEnvironmentVariable ("KEYSTORE_ALIAS_PASSWORD");
		
		//Set valid Android SDK
		EditorPrefs.SetString("AndroidSdkRoot", Environment.GetEnvironmentVariable ("ANDROID_HOME"));

		//Create scenes array
		EditorBuildSettingsScene[] scenes = EditorBuildSettings.scenes;
		string[] scenespath = new string[scenes.Length];
		for (int i = 0; i < scenes.Length; i++)
			scenespath[i] = scenes[i].path;

		//Setup build options
		BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions ();
		buildPlayerOptions.scenes = scenespath;
		buildPlayerOptions.targetGroup = BuildTargetGroup.Android;
		buildPlayerOptions.target = BuildTarget.Android;
		buildPlayerOptions.locationPathName = PlayerSettings.productName + "_" + branch + "_" + commit + ".apk";
		buildPlayerOptions.options = BuildOptions.CompressWithLz4HC | BuildOptions.StrictMode;

		//Build
		Console.WriteLine ("===== START BUILD (" + branch + "+" + commit + ") =====");
		BuildReport report = BuildPipeline.BuildPlayer (buildPlayerOptions);
		
		//Check
		BuildSummary summary = report.summary;

		if (summary.result == BuildResult.Succeeded && File.Exists(buildPlayerOptions.locationPathName))
			Console.WriteLine ("===== SUCCESSFUL BUILD =====");
		else
		{
			Console.WriteLine ("===== UNSUCCESSFUL BUILD =====");
			EditorApplication.Exit (1);
		}
	}
}
